import os
import time

from flask import request
from flask import Flask, render_template
import mysql.connector
from mysql.connector import errorcode

application = Flask(__name__)
app = application


def get_db_creds():
    db = os.environ.get("DB", None) or os.environ.get("database", None)
    username = os.environ.get("USER", None) or os.environ.get("username", None)
    password = os.environ.get("PASSWORD", None) or os.environ.get("password", None)
    hostname = os.environ.get("HOST", None) or os.environ.get("dbhost", None)
    return db, username, password, hostname


def create_table():
    # Check if table exists or not. Create and populate it only if it does not exist.
    db, username, password, hostname = get_db_creds()
    table_ddl = 'CREATE TABLE movies(id INT UNSIGNED NOT NULL AUTO_INCREMENT, year INT, title VARCHAR(255), director VARCHAR(255), actor VARCHAR(255), release_date VARCHAR(255), rating DOUBLE, lcase_title VARCHAR(255), lcase_actor VARCHAR(255), PRIMARY KEY (id))'

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)

    cur = cnx.cursor()

    try:
        cur.execute(table_ddl)
        cnx.commit()
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
            print("already exists.")
        else:
            print(err.msg)

def query_data():

    db, username, password, hostname = get_db_creds()

    print("Inside query_data")
    print("DB: %s" % db)
    print("Username: %s" % username)
    print("Password: %s" % password)
    print("Hostname: %s" % hostname)

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)

    cur = cnx.cursor()

    cur.execute("SELECT title FROM movies")
    entries = [dict(movie=row[0]) for row in cur.fetchall()]
    return entries

try:
    print("---------" + time.strftime('%a %H:%M:%S'))
    print("Before create_table")
    create_table()
    print("After create_table")
except Exception as exp:
    print("Got exception %s" % exp)
    conn = None


@app.route('/add_movie', methods=['POST'])
def add_movie():
    print("Received request to insert movie.")
    print(request.form)
    year = request.form['year']
    title = request.form['title']
    director = request.form['director']
    actor = request.form['actor']
    release_date = request.form['release_date']
    rating = request.form['rating']
    lcase_title = title.lower()
    lcase_actor = actor.lower()

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    try:
        cur = cnx.cursor()
        query = "SELECT title from movies WHERE lcase_title=%s"
        cur.execute(query, (title.lower(),))
        entries = [dict(movie=row[0]) for row in cur.fetchall()]
        print("Check if movie already exists")
        print("Result: " + str(entries))

        if len(entries) > 0:
            message = "Movie " + title + " could not be inserted - movie already exists"
            return render_template('index.html', message=[message])

        cur.execute("INSERT INTO movies (year,title,director,actor,release_date,rating,lcase_title,lcase_actor) VALUES (%s,%s,%s,%s,%s,%s,%s,%s)", (int(year), title, director, actor, release_date, float(rating), lcase_title, lcase_actor))
        cnx.commit()
    except Exception as e:
        message= "Movie " + title + " could not be inserted - " + str(e)
        return render_template('index.html', message=[message])
    
    message = "Movie " + title + " successfully inserted"
    return render_template('index.html', message=[message])

@app.route('/update_movie', methods=['POST'])
def update_movie():
    print("Received request to update movie!")
    print(request.form)
    year = request.form['year']
    title = request.form['title']
    director = request.form['director']
    actor = request.form['actor']
    release_date = request.form['release_date']
    rating = request.form['rating']
    lcase_title = title.lower()
    lcase_actor = actor.lower()

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    try:
        cur = cnx.cursor()
        query = "SELECT title from movies WHERE lcase_title=%s"
        cur.execute(query, (title.lower(),))
        entries = [dict(movie=row[0]) for row in cur.fetchall()]
        print("Result: " + str(entries))

        if len(entries) == 0:
            message = "Movie " + title + " does not exist"
            return render_template('index.html', message=[message])

        
        cur.execute("UPDATE movies SET year=%s,title=%s,director=%s,actor=%s,release_date=%s,rating=%s,lcase_title=%s,lcase_actor=%s where title=%s", (int(year), title, director, actor, release_date, float(rating), lcase_title, lcase_actor, title))
        cnx.commit()
    except Exception as e:
        message= "Movie " + title + " could not be updated - " + str(e)
        return render_template('index.html', message=[message])
    
    message = "Movie " + title + " successfully updated"
    return render_template('index.html', message=[message])    

@app.route("/search_movie", methods=['POST'])
def search_movie():
    print("Received search_movie request")
    print(request.form)
    actor = request.form['search_actor']

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    try:
        cur = cnx.cursor()
        print("Actor: " + actor)
        query = "SELECT title, year, actor From movies WHERE lcase_actor=%s"
        cur.execute(query,(actor.lower(),))
           
        #entries = [dict(movie=row[0]) for row in cur.fetchall()]
        entries = cur.fetchall()
        print("Result: " + str(entries))
        if len(entries)==0:
            message = "No movies found for actor " + actor
            return render_template('index.html', message=[message])

        total = []
        for entry in entries:
            message = ""
            for i in entry:
                message += str(i) + ", "
            message=message[0:len(message)-2]
            total.append(message)
        return render_template('index.html', message=total)
        
    except Exception as e:
        print("Exception: " + str(e))
        message= "No movies found for actor " + actor + " - " + str(e)
        return render_template('index.html', message=[message])


@app.route("/delete_movie", methods=['POST'])
def delete_movie():
    print("Received delete_movie request")
    print(request.form)
    title = request.form['delete_title']
    print("title: " + title)
    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    try:
        cur = cnx.cursor()
        query = "SELECT title from movies WHERE lcase_title=%s"
        cur.execute(query, (title.lower(),))
        entries = [dict(movie=row[0]) for row in cur.fetchall()]
        print("Result: " + str(entries))

        if len(entries) == 0:
            message = "Movie " + title + " does not exist"
            return render_template('index.html', message=[message])

        query = "DELETE From movies WHERE lcase_title=%s"
        cur.execute(query, (title.lower(),))
        cnx.commit()
        message = "Movie " + title + " successfully deleted!"
        return render_template('index.html', message=[message])
    except Exception as e:  
        print("Exception: " + str(e))
        message= "Movie with title " + title + " could not be deleted - " + str(e)
        return render_template('index.html', message=[message])
    

@app.route("/highest_rating", methods=['GET'])
def highest_rating():
    print("Received highest_rating request")

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    try:
        cur = cnx.cursor()
        cur.execute("SELECT title, year, actor, director,rating from movies where rating=(select max(rating) FROM movies)")
        entries= cur.fetchall()
        #entries = [dict(movie=row[0]) for row in cur.fetchall()]
        print("Entries: " + str(entries))
        total = []
        for entry in entries:
            message = ""
            for i in entry:
                message += str(i) + ", "
            message=message[0:len(message)-2]
            total.append(message)
        return render_template('index.html', message=total)
    except Exception as e:
        print("Exception: " + str(e))
        message= "Max rating could not be found - " + str(e)
        return render_template('index.html', message=[message])
    
@app.route("/lowest_rating", methods=['GET'])
def lowest_rating():
    print("Received lowest_rating request")

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    try:
        cur = cnx.cursor()
        cur.execute("SELECT title, year, actor, director,rating from movies where rating=(select min(rating) FROM movies)")
        entries = cur.fetchall()
        print("Entries: " + str(entries))
        total = []
        for entry in entries:
            message = ""
            for i in entry:
                message += str(i) + ", "
            message=message[0:len(message)-2]
            total.append(message)
        return render_template('index.html', message=total)
    except Exception as e:
        print("Exception: " + str(e))
        message= "Min rating could not be found - " + str(e)
        return render_template('index.html', message=[message])

@app.route("/")
def hello():
    print("Printing available environment variables")
    print(os.environ)
    print("Before displaying index.html")
    movies = query_data()
    print("Movies: %s" % movies)
    return render_template('index.html', entries=movies)


if __name__ == "__main__":
    app.debug = True
    app.run(host='0.0.0.0')
